using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace PreLiteralsAnalyzer
{
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class PreLiteralsAnalyzerAnalyzer : DiagnosticAnalyzer
    {
        public const string DiagnosticId = "PreLiteralsAnalyzer";

        // You can change these strings in the Resources.resx file. If you do not want your analyzer to be localize-able, you can use regular strings for Title and MessageFormat.
        // See https://github.com/dotnet/roslyn/blob/master/docs/analyzers/Localizing%20Analyzers.md for more on localization
        private static readonly LocalizableString Title = new LocalizableResourceString(nameof(Resources.AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        public static readonly LocalizableString MessageFormat = new LocalizableResourceString(nameof(Resources.AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Description = new LocalizableResourceString(nameof(Resources.AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private const string Category = "CodeStyle";

        private static DiagnosticDescriptor Rule = new DiagnosticDescriptor(DiagnosticId, Title, MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: Description);

        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics { get { return ImmutableArray.Create(Rule); } }

        public override void Initialize(AnalysisContext context)
        {
            // TODO: Consider registering other actions that act on syntax instead of or in addition to symbols
            // See https://github.com/dotnet/roslyn/blob/master/docs/analyzers/Analyzer%20Actions%20Semantics.md for more information
            context.RegisterSyntaxNodeAction(StringLiteralUsing, SyntaxKind.StringLiteralExpression);
        }

        private static void StringLiteralUsing(SyntaxNodeAnalysisContext context)
        {
            // TODO: Replace the following code with your own analysis, generating Diagnostic objects for any issues you find
            if (context.Node.Parent?.Parent?.Parent?.Parent?.Kind() == SyntaxKind.FieldDeclaration)
                return;

            var stringLiteralOperation = (LiteralExpressionSyntax)context.Node;

            // Find just those named type symbols with names containing lowercase letters.

            // For all such symbols, produce a diagnostic.
            var diagnostic = Diagnostic.Create(Rule, stringLiteralOperation.GetLocation());

            context.ReportDiagnostic(diagnostic);

        }
    }
}
