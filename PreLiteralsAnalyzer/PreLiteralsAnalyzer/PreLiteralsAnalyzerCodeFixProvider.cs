using System.Collections.Generic;
using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Text;
using System.Text.RegularExpressions;

namespace PreLiteralsAnalyzer
{
    [ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(PreLiteralsAnalyzerCodeFixProvider)), Shared]
    public class PreLiteralsAnalyzerCodeFixProvider : CodeFixProvider
    {
        private const string title = "You can use constants";

        private Dictionary<SourceText, IdentifierNameSyntax> _replacedLiteralsByIdentifier = new Dictionary<SourceText, IdentifierNameSyntax>();

        private SyntaxNode _classNode;

        private SyntaxNode _root;

        public sealed override ImmutableArray<string> FixableDiagnosticIds
        {
            get { return ImmutableArray.Create(PreLiteralsAnalyzerAnalyzer.DiagnosticId); }
        }

        public sealed override FixAllProvider GetFixAllProvider()
        {
            // See https://github.com/dotnet/roslyn/blob/master/docs/analyzers/FixAllProvider.md for more information on Fix All Providers
            return WellKnownFixAllProviders.BatchFixer;
        }

        public sealed override async Task RegisterCodeFixesAsync(CodeFixContext context)
        {
            _root = await context.Document.GetSyntaxRootAsync(context.CancellationToken).ConfigureAwait(false);

            // TODO: Replace the following code with your own analysis, generating a CodeAction for each fix to suggest
            var diagnostic = context.Diagnostics.First();
            var diagnosticSpan = diagnostic.Location.SourceSpan;

            // Find the type declaration identified by the diagnostic.
            var declaration = _root.FindToken(diagnosticSpan.Start).Parent.AncestorsAndSelf().OfType<LiteralExpressionSyntax>().First();

            // Register a code action that will invoke the fix.
            context.RegisterCodeFix(
                CodeAction.Create(
                    title: title,
                    createChangedDocument: c => ChangeToConstAsync(context.Document, declaration, c), 
                    equivalenceKey: title),
                diagnostic);
        }


        private async Task<Document> ChangeToConstAsync(Document document, LiteralExpressionSyntax declaration, CancellationToken cancellationToken)
        {

            var identifierToken = declaration.GetText();

            _root = await document.GetSyntaxRootAsync(cancellationToken);

            _classNode = declaration.Ancestors().OfType<ClassDeclarationSyntax>().FirstOrDefault();

            if (!UsedExistFieldsInClass(declaration))
            {
                var constName = GetConstNameBySourceText(identifierToken);

                var constNameToken = SyntaxFactory.Identifier(SyntaxTriviaList.Empty, SyntaxKind.IdentifierToken, constName, constName, SyntaxTriviaList.Empty)
                    .WithTrailingTrivia(SyntaxFactory.Whitespace(" "));

                var equalsToken = SyntaxFactory.Token(SyntaxKind.EqualsToken)
                    .WithTrailingTrivia(SyntaxFactory.Whitespace(" "));
                var equalsValueClause = SyntaxFactory.EqualsValueClause(equalsToken, declaration);

                var variableDeclarator = SyntaxFactory.VariableDeclarator(constNameToken, null, equalsValueClause);
                var stringKeywordToken = SyntaxFactory.Token(SyntaxKind.StringKeyword)
                    .WithTrailingTrivia(SyntaxFactory.Whitespace(" "));
                var predefinedType = SyntaxFactory.PredefinedType(stringKeywordToken);

                var separatedSyntaxList = SyntaxFactory.SeparatedList<VariableDeclaratorSyntax>().Add(variableDeclarator);

                var variableDeclaration = SyntaxFactory.VariableDeclaration(predefinedType, separatedSyntaxList);


                var semicolonToken = SyntaxFactory.Token(SyntaxKind.SemicolonToken)
                    .WithTrailingTrivia(SyntaxFactory.EndOfLine(string.Empty));
                var constKeywordToken = SyntaxFactory.Token(SyntaxKind.ConstKeyword)
                    .WithTrailingTrivia(SyntaxFactory.Whitespace(" "));
                var privateKeywordToken = SyntaxFactory.Token(SyntaxKind.PrivateKeyword)
                    .WithTrailingTrivia(SyntaxFactory.Whitespace(" "));


                var syntaxTokenList = SyntaxFactory.TokenList(privateKeywordToken, constKeywordToken);
                var fieldDeclaration = SyntaxFactory.FieldDeclaration(new SyntaxList<AttributeListSyntax>(), syntaxTokenList, variableDeclaration, semicolonToken);


                var constNameSyntax = SyntaxFactory.IdentifierName(constNameToken).NormalizeWhitespace();
                var classNodeWithChangedDeclaration = _classNode.ReplaceNode(declaration, constNameSyntax);


                var nodeBeforeInserted = classNodeWithChangedDeclaration.DescendantNodes().First();
                var newNodes = new List<SyntaxNode>() { fieldDeclaration.WithTriviaFrom(nodeBeforeInserted) };

                var classWithConst = classNodeWithChangedDeclaration.InsertNodesBefore(nodeBeforeInserted, newNodes);

                _root = _root.ReplaceNode(_classNode, classWithConst);
            }
            return document.WithSyntaxRoot(_root);
        }

        private bool UsedExistFieldsInClass(LiteralExpressionSyntax declaration)
        {
            bool usedExistFields = false;
            var fields = _classNode.ChildNodes().Where(n => n.IsKind(SyntaxKind.FieldDeclaration));
            if (fields != null)
            {

                var literals = fields.SelectMany(field => field.DescendantNodes().OfType<LiteralExpressionSyntax>());
                if (literals != null)
                {
                    var targetLiteralExpression = literals.FirstOrDefault(literal => literal.IsEquivalentTo(declaration));
                    if (targetLiteralExpression != null)
                    {

                        var targetVariable = targetLiteralExpression.Ancestors().OfType<VariableDeclaratorSyntax>().First();
                        var targetFieldModifiers = targetLiteralExpression.Ancestors().OfType<FieldDeclarationSyntax>().First().Modifiers;
                        var targetVariableIsConst = targetFieldModifiers.Any(SyntaxKind.ConstKeyword);

                        //check if method, what contains literalDecl is static 
                        var targetMethodIsStatic = declaration.Ancestors().OfType<MethodDeclarationSyntax>().First().Modifiers.Any(m => m.IsKind(SyntaxKind.StaticKeyword));

                        //check if field is static
                        var targetFieldIsStatic = targetFieldModifiers.Any(m => m.IsKind(SyntaxKind.StaticKeyword));

                        if (targetVariableIsConst || !targetMethodIsStatic || targetFieldIsStatic)
                        {

                            var identifierToChange = targetVariable.Identifier;
                            var constNameSyntaxExpression = SyntaxFactory.IdentifierName(identifierToChange).NormalizeWhitespace();
                            _root = _root.ReplaceNode(declaration, constNameSyntaxExpression);
                            usedExistFields = true;
                        }
                    }
                }
            }

            return usedExistFields;
        }

        private string GetConstNameBySourceText(SourceText identifierToken)
        {
            var nonWordPattern = "\\W+";
            var regEx = new Regex(nonWordPattern);
            var constName = regEx.Replace(identifierToken.Lines.First().Text.ToString().Trim('"'), "_").Trim('_').ToUpperInvariant();

            regEx = new Regex("^\\d");
            if (regEx.IsMatch(constName))
            {
                constName = '_' + constName;
            }
            return constName;
        }
    }
}
