using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestHelper;
using PreLiteralsAnalyzer;

namespace PreLiteralsAnalyzer.Test
{
    [TestClass]
    public class UnitTest : CodeFixVerifier
    {

        //No diagnostics expected to show up
        //[TestMethod]
        //public void TestMethod1()
        //{
        //    var test = @"";

        //    VerifyCSharpDiagnostic(test);
        //}

        //    //Diagnostic and CodeFix both triggered and checked for
        //    [TestMethod]
        //    public void TestMethod2()
        //    {
        //        var test = @"
        //using System;
        //using System.Collections.Generic;
        //using System.Linq;
        //using System.Text;
        //using System.Threading.Tasks;
        //using System.Diagnostics;

        //namespace ConsoleApplication1
        //{
        //    class TypeName
        //    {   
        //    }
        //}";
        //        var expected = new DiagnosticResult
        //        {
        //            Id = "PreLiteralsAnalyzer",
        //            Message = String.Format("Type name '{0}' contains lowercase letters", "TypeName"),
        //            Severity = DiagnosticSeverity.Warning,
        //            Locations =
        //                new[] {
        //                        new DiagnosticResultLocation("Test0.cs", 11, 15)
        //                    }
        //        };

        //        VerifyCSharpDiagnostic(test, expected);

        //        var fixtest = @"
        //using System;
        //using System.Collections.Generic;
        //using System.Linq;
        //using System.Text;
        //using System.Threading.Tasks;
        //using System.Diagnostics;

        //namespace ConsoleApplication1
        //{
        //    class TYPENAME
        //    {   
        //    }
        //}";
        //        VerifyCSharpFix(test, fixtest);
        //    }

        [TestMethod]
        public void StringLiteralWarning()
        {

            var test = @"
namespace ConsoleApplication1 {
  class Example {
    private const string HELLO_WORLD = ""Hello World!""
    public static void Run() {
     var s = ""Hello"";
    }   
  }
}";
            var expected = new DiagnosticResult
            {
                Id = PreLiteralsAnalyzerAnalyzer.DiagnosticId,
                Message = PreLiteralsAnalyzerAnalyzer.MessageFormat.ToString(),
                Severity = DiagnosticSeverity.Warning,
                Locations = new[] { new DiagnosticResultLocation("Test0.cs", 6, 14) }
            };

            VerifyCSharpDiagnostic(test, expected);


        }

        [TestMethod]
        public void StringLiteralFixerTest()
        {

            var code = @"
using System;
using System.Threading;
using System.Threading.Tasks;

namespace example
{
    public class Some
    {
        public void Operation()
            {
                Console.WriteLine($""Operation ThreadID {Thread.CurrentThread.ManagedThreadId}"");
                Console.WriteLine(""Begin"");
                Thread.Sleep(2000);
                Console.WriteLine(""End"");
            }

            public async void OperationAsync()
            {
                Task task = new Task(Operation);
                task.Start();
                await task;
            }
        }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(""Hello World!"");
        }
    }
}
";
            var fixedCode = @"
using System;
using System.Threading;
using System.Threading.Tasks;

namespace example
{
    public class Some
    {
        private const string END = ""End"";
        private const string BEGIN = ""Begin"";
        public void Operation()
            {
                Console.WriteLine($""Operation ThreadID {Thread.CurrentThread.ManagedThreadId}"");
                Console.WriteLine(BEGIN);
                Thread.Sleep(2000);
                Console.WriteLine(END);
            }

            public async void OperationAsync()
            {
                Task task = new Task(Operation);
                task.Start();
                await task;
            }
        }

    class Program
    {
        private const string HELLO_WORLD = ""Hello World!"";
        static void Main(string[] args)
        {
            Console.WriteLine(HELLO_WORLD);
        }
    }
}
";


            VerifyCSharpFix(code, fixedCode);


        }

        [TestMethod]
        public void StringLiteralFixerWithExistStringFieldsTest()
        {

            var code = @"
using System;
using System.Threading;
using System.Threading.Tasks;

namespace example
{
    public class Some
    {
        private const string BEGIN = ""Begin"";
        public void Operation()
            {
                Console.WriteLine($""Operation ThreadID {Thread.CurrentThread.ManagedThreadId}"");
                Console.WriteLine(""Begin"");
                Thread.Sleep(2000);
                Console.WriteLine(""End"");
            }

            public async void OperationAsync()
            {
                Task task = new Task(Operation);
                task.Start();
                await task;
            }
        }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(""Hello World!"");
        }
    }
}
";
            var fixedCode = @"
using System;
using System.Threading;
using System.Threading.Tasks;

namespace example
{
    public class Some
    {
        private const string END = ""End"";
        private const string BEGIN = ""Begin"";
        public void Operation()
            {
                Console.WriteLine($""Operation ThreadID {Thread.CurrentThread.ManagedThreadId}"");
                Console.WriteLine(BEGIN);
                Thread.Sleep(2000);
                Console.WriteLine(END);
            }

            public async void OperationAsync()
            {
                Task task = new Task(Operation);
                task.Start();
                await task;
            }
        }

    class Program
    {
        private const string HELLO_WORLD = ""Hello World!"";
        static void Main(string[] args)
        {
            Console.WriteLine(HELLO_WORLD);
        }
    }
}
";


            VerifyCSharpFix(code, fixedCode);


        }

        protected override CodeFixProvider GetCSharpCodeFixProvider()
        {
            return new PreLiteralsAnalyzerCodeFixProvider();
        }

        protected override DiagnosticAnalyzer GetCSharpDiagnosticAnalyzer()
        {
            return new PreLiteralsAnalyzerAnalyzer();
        }
    }
}
